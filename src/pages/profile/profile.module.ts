import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBqOTpdm_dhZ59MZGvFHSN7e21XeYcS1sM'
    })
  ],
})
export class ProfilePageModule {}
