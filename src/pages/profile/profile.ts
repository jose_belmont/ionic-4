import { EventProvider } from './../../providers/event/event';
import { Observable } from 'rxjs/Observable';
import { UserProvider } from './../../providers/user/user';
import { DomSanitizer } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IUser } from '../../interfaces/i-user';
import { constants } from '../../constants';
import { IEvent } from '../../interfaces/i-event';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { IResponse } from '../../interfaces/i-response';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user: IUser = {
    image: "",
    lat: 38.4039418,
    lng: -0.5288701,
    name: "",
    email: "",
    password: ""
  };
  my: boolean;
  eventAt : any [];
  eventCre : IEvent[];
  lat = 38.4039418;
  lng = -0.5288701;
  zoom = 17;
  id = this.user.id;
  content: string;

itsMe: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public domSanitizer: DomSanitizer,
    public userProvider: UserProvider,
    public eventProvider: EventProvider,
    public http : HttpClient) {
    console.log('email:', this.navParams.data.email);
    this.content = "info";
    if (this.navParams.data.email) {
      this.user = this.navParams.data;
      this.itsMe = false;
    } else {
      this.getMyProfile();
      this.itsMe = true;

    };
  }

  ionViewDidLoad() {

    console.log('lat y lng:', typeof (this.lat), typeof (this.user.lng));
  }

  goEdit() {
    this.navCtrl.push('RegisterPage',this.user);
  }
  getMyProfile() {
    this.userProvider.getMyProfile().subscribe(result => {
      this.user = result[0];
      this.lat = +this.user.lat;
      this.lng = +this.user.lng;
      this.user.image = `${constants.imgUsers}${this.user.image}`;
      
      console.log('my profile', this.user);
      this.getEventAtt();
      this.getEventCreate();
    });
  }

  getEventAtt() {
    this.eventProvider.getEventsAttends(this.user.id).subscribe( result => {
      console.log('Att');
      console.log(result);
      this.eventAt = result;
    });
  }

  getEventCreate() {
    this.eventProvider.getEventsCreated(this.user.id).subscribe(result => {
      console.log('Even');
      console.log(result);
      this.eventCre = result;
    });
  }

}
