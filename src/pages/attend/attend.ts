import { EventProvider } from './../../providers/event/event';
import { IEvent } from './../../interfaces/i-event';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AttendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-attend',
  templateUrl: 'attend.html',
})
export class AttendPage {
  event: IEvent = {
    id: 0,
    title: "",
    date: "",
    description: "",
    image: "",
    price: 0,
    address: "",
    lat: 0,
    lng: 0
  };

  users: any[];


  constructor(public navCtrl: NavController, public navParams: NavParams, public eventProvider: EventProvider) { this.event = this.navParams.data; }

  getAttends() {
    this.eventProvider.getAttendsEventUser(this.event.id).subscribe(result => {
      this.users = result;
      console.log(result);
    });
  }

  ionViewDidLoad() {
    this.getAttends();
    console.log(this.users);
  }

  profile(event) {
    if (!event.mine) {
      this.navCtrl.push('ProfilePage', event);
    }
    else {
      this.navCtrl.push('ProfilePage');
    }
  }

}
