import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Refresher } from "ionic-angular";
import { EventProvider } from "../../providers/event/event";
import { IEvent } from "../../interfaces/i-event";
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the EventListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-event-list",
  templateUrl: "event-list.html"
})
export class EventListPage {
  events: IEvent[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public eventProvider: EventProvider,
    public domSanitizer : DomSanitizer
  ) { }

  ionViewDidLoad() {
    this.getEvents();
  }

  getEvents() {
    this.eventProvider.getEvents().subscribe(allEvents => {
      this.events = allEvents;
    });
  }

  profile(event){  
      if (!event.mine) {
        this.navCtrl.push('ProfilePage', event);
      }
      else {
        this.navCtrl.push('ProfilePage');
    }
  }

  eventDetail(event) {
    console.log(event);
    this.navCtrl.push('EventDetailsPage', event);
  }

  refresh(refresher: Refresher) {
    this.getEvents();
    refresher.complete();
  }
}
