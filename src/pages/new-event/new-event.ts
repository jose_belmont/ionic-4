import { Component } from "@angular/core";
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { IEvent } from "../../interfaces/i-event";
import { EventProvider } from "../../providers/event/event";

import { Camera, CameraOptions } from "@ionic-native/camera";

import { AgmCoreModule } from '@agm/core';

/* import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
 */

/**
 * Generated class for the NewEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-new-event",
  templateUrl: "new-event.html"
})
export class NewEventPage {
  event: IEvent = {
    title: "",
    date: "",
    description: "",
    image: "",
    price: 0,
    address: "",
    lat: 0,
    lng: 0
  };

  lat = 38.4039418;
  lng = -0.5288701;
  zoom = 17;

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public eventProvider: EventProvider,
    public camera: Camera,
/*     public launchNavigator: LaunchNavigator
 */  ) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NewEventPage");
  }

  cameraUse() {
    this.camera.getPicture(this.options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        this.event.image = base64Image;
        this.alertCtrl
          .create({
            title: "Success",
            subTitle: "Image captured correctly",
            buttons: ["OK"]
          })
          .present();
      },
      err => {
        this.alertCtrl
          .create({
            title: "Error",
            subTitle: "Camera Error",
            buttons: ["OK"]
          })
          .present();
      }
    );
  }

  create() {
    this.eventProvider.addEvent(this.event).subscribe(
      response => {
        this.navCtrl.pop();
      },
      error => {
        this.alertCtrl.create({
          title: "Error",
          subTitle: error,
          buttons: ["Ok"]
        });
      }
    );
  }

  /* navigate() {
    let options: LaunchNavigatorOptions = {};
    this.launchNavigator.navigate([this.lat, this.lng], options).then(ok => console.log("Navigation launched!"));
  } */
}