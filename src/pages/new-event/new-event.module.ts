import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewEventPage } from './new-event';

@NgModule({
  declarations: [
    NewEventPage,
  ],
  imports: [
    IonicPageModule.forChild(NewEventPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBqOTpdm_dhZ59MZGvFHSN7e21XeYcS1sM'
    })
  ],
})
export class NewEventPageModule {}
