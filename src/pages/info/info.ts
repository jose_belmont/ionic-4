import { IEvent } from './../../interfaces/i-event';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  event: IEvent = {
    title: "",
    date: "",
    description: "",
    image: "",
    price: 0,
    address: "",
    lat: 0,
    lng: 0
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public domSanitizer: DomSanitizer) {
    console.log(this.navParams);
    this.event = this.navParams.data;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }
  profile(event) {
    if (!event.mine) {
      this.navCtrl.push('ProfilePage', event);
    }
    else {
      this.navCtrl.push('ProfilePage');
    }
  }
}
