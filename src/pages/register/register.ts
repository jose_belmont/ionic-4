import { Camera, CameraOptions } from '@ionic-native/camera';
import { AuthProvider } from './../../providers/auth/auth';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUser } from './../../interfaces/i-user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { constants } from '../../constants';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }
  user: IUser = {
    image: "",
    lat: 0,
    lng: 0,
    name: "",
    email: "",
    password: "",
    password2: ""
  };
  pass: boolean;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private authProvider: AuthProvider,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
    private camera: Camera) {

    if (this.navParams.data.email) {
      this.user = this.navParams.data;
      this.pass = false;
      this.user.password = this.navParams.data.password;
      this.user.password2 = this.navParams.data.password2;

      this.register()

    } else if(!this.navParams.data.id) {
      
      this.user = this.navParams.data;
      this.pass = true;
      
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  ionViewWillLoad() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.user.lat = resp.coords.latitude;
      this.user.lng = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  register() {
    if (this.user.password === this.user.password2) {
      this.authProvider.register(this.user).subscribe(
        () => this.navCtrl.setRoot('EventListPage'),
        (error) => this.showErrorRegister(error)
      );
    }

    else {
      let alert = this.alertCtrl.create({
        title: 'Password error or not match',
        subTitle: 'Password error or not match',
        buttons: ['Ok']
      });
      alert.present();
    }
  }

  private showErrorRegister(error) {
    let alert = this.alertCtrl.create({
      title: 'Login error',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

  cameraUse() {
    
    this.camera.getPicture(this.options).then(
      imageData => {
        let base64Image = "data:image/jpeg;base64," + imageData;
        this.user.image = base64Image;
        this.alertCtrl
          .create({
            title: "Success",
            subTitle: "Image captured correctly",
            buttons: ["OK"]
          })
          .present();
      },
      err => {
        this.alertCtrl
          .create({
            title: "Error",
            subTitle: "Camera Error",
            buttons: ["OK"]
          })
          .present();
      }
    );
    
  }

}

