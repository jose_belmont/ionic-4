import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationPage } from './location';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    LocationPage,
  ],
  imports: [
    IonicPageModule.forChild(LocationPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBqOTpdm_dhZ59MZGvFHSN7e21XeYcS1sM'
    })
  ],
})
export class LocationPageModule {}
