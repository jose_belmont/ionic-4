import { IEvent } from './../../interfaces/i-event';
import { DomSanitizer } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AgmCoreModule } from '@agm/core';


/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  event: IEvent = {
    title: "",
    date: "",
    description: "",
    image: "",
    price: 0,
    address: "",
    lat: 0,
    lng: 0
  };
  lat = 38.4039418;
  lng = -0.5288701;
  zoom = 17;

  constructor(public navCtrl: NavController, public navParams: NavParams, public domSanitizer: DomSanitizer) {
    console.log(this.navParams);
    this.event = this.navParams.data;

  }

  ionViewDidLoad() {
    this.lat = +this.event.lat;
    this.lng = +this.event.lng;
  }

}
