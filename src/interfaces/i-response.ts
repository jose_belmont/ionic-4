export interface IResponse {
    error: boolean,
    errorMessage: string,
    token: string,
    result: any
}