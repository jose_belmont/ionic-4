export interface IUser{
    id?:number,
    name:string,
    email:string,
    password:string,
    password2?:string,
    image?:string,
    lat?:number,
    lng?:number,
    id_Facebook?:string,
    id_Google?:string
}