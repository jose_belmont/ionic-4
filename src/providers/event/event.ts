import { IUser } from './../../interfaces/i-user';
import { IResponse } from './../../interfaces/i-response';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { constants } from './../../constants';
import { Observable } from 'rxjs/Observable';
import { IEvent } from './../../interfaces/i-event';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

/*
  Generated class for the EventProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventProvider {

server:string = constants.server;
eventsImg:string = constants.imgEvents;
usersImg: string = constants.imgUsers;

  constructor(public http: HttpClient,
  private sanitizer: DomSanitizer) {
    console.log('Hello EventProvider Provider');
  }

  // sanitize(url: string) {
  //   return this.sanitizer.bypassSecurityTrustUrl(url);
  // }
  // urlServer = this.sanitize(this.server).toString();
  // urlImgEvents = this.sanitize(this.eventsImg).toString();
  // urlImgUsers = this.sanitize(this.usersImg).toString();
  

  addEvent(event: IEvent): Observable<boolean> {
    // console.log(event);
    return this.http
      .post(this.server.substr(0,8), event)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw('Error adding!')
      )
      .map((resp: IResponse) => {
        if (resp.error) {
          throw resp.errorMessage;
        }
        return true;
      });
  }

  getEvents(): Observable<IEvent[]> {
    console.log(`${this.server}events`);
    return this.http
      .get(`${this.server}events`)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          'Error getting events!' +
          `. Server returned code ${resp.status}, message was: ${resp.message}`
        )
      )
      .map((resp: IResponse) => {
        return resp.result.map((event: IEvent) => {
          event.image = `${this.eventsImg}${event.image}`;
          event.creatorData.image = `${this.usersImg}${event.creatorData.image}`;
          return event;
        });
      });
  }

  getEvent(id: number): Observable<IEvent> {
    return this.http
      .get(`${this.server}events/${id}`)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          'Error getting events!' +
          `. Server returned code ${resp.status}, message was: ${resp.message}`
        )
      )
      .map((resp: IResponse) => {
        resp.result.image = `${this.eventsImg}${resp.result.image}`;
        resp.result.creatorData.image = `${this.usersImg}${resp.result.creatorData.image}`;
        return resp.result;
      });
  }

  getAttendsEventUser(id: number): Observable<IUser[]> {
    return this.http.get(`${constants.server}events/attend/${id}`)
      .catch((error: HttpErrorResponse) => Observable.throw(`Error trying to get attends events. Server returned ${error.message}`))
      .map((resp: IResponse) => {
        if (!resp.error) {
          resp.result.forEach((user: any) => {
            // user.image = Environment.imageUrlUser + user.image;
            user.image = `${this.eventsImg}${user.image}`;

          });
          return resp.result;
        }
        throw resp.errorMessage;
      });
  }

  getEventsAttends(idUser: Number): Observable<IEvent[]> {
    return this.http
      .get(constants.server + "users/attend/" + idUser)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          "Error trying to get events. Server returned" + error.message
        )
      )
      .map((resp: IResponse) => {
        resp.result.forEach((event: any) => {
          event.eventData.image =
            constants.imgEvents + event.eventData.image;
        });
        return resp.result;
      });
  }

  getEventsCreated(idUser: number): Observable<IEvent[]> {
    return this.http
      .get(constants.server + "events/created/" + idUser)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          `Error trying to get events. Server returned ${error.message}`
        )
      )
      .map((resp: IResponse) => {
        resp.result.forEach((event: IEvent) => {
          event.image = constants.imgEvents + event.image;
        });
        return resp.result;
      });
  }

  removeEvent(id: number): Observable<boolean> {
    return this.http
      .delete(constants.server + "events/" + id)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          `Error trying to get events. Server returned ${error.message}`
        )
      )
      .map((resp: IResponse) => {
        if (!resp.error) {
          return true;
        }
        throw resp.errorMessage;
      });
  }

  

}
