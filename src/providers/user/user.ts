import { constants } from './../../constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IResponse } from '../../interfaces/i-response';
import { Observable } from 'rxjs/Observable';
import { IUser } from '../../interfaces/i-user';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  server: string = constants.server;
  eventsImg: string = constants.imgEvents;
  usersImg: string = constants.imgUsers;
  
  constructor(public http: HttpClient,
    private sanitizer: DomSanitizer) {
    console.log('Hello UserProvider Provider');
  }

  getUser(id: number): Observable<IUser> {
    return this.http
      .get(`${this.server}users/${id}`)
      .catch(error => Observable.throw("Error trying to get user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          user.result.image = `${this.usersImg}${user.result.image}`;
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  getMyProfile(): Observable<IUser> {
    return this.http.get(`${this.server}users/me`)
      .catch(error => Observable.throw("Error trying to get user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          user.result.image =`${this.usersImg}${user.result.image}`;
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  updateProfile(user: IUser): Observable<IUser> {
    return this.http.put(constants.server + "users/" + user.id, user)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }


}
